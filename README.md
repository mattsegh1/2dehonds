![Imgur](https://i.imgur.com/X6Ly5E6.png)
# 2dehonds
Is een webplatform omtrent asieldieren. Het zorgt ervoor dat mensen doelgericht een asieldier kunnen zoeken en contact met het asiel opnemen en bovendien informatie kunnen vinden over hoe men om moet gaan met een asieldier.

### Vereisten

* composer
* gulp
* PHP 7
* package manager (bv: yarn)

```
Give examples
```

### Instalatie

Haal de laatste versie van de applicatie binnen.
```
git pull git@bitbucket.org:mattsegh1/2dehonds.git
```
Installeer de packages
```
composer install
```
Volg de instructies op het scherm.

De database opzetten kan je met
```
php bin/console 2dehonds:setup
```

Om de database van data te voorzien gebruik je onderstaand commando
```
php bin/console doctrine:fixtures:load
```

## Deployment

Add additional notes about how to deploy this on a live system

## Authors

* **Matthias Seghers**