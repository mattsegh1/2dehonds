<?php

namespace AppBundle\Repository;

/**
 * PostRepository
 *
 * @author Matthias Seghers
 */
class PostRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @return array
     */
    public function findLatest($amount = 1)
    {
        return $this->createQueryBuilder('p')
            ->join('p.user', 'u')->addSelect('u')
            ->where('p.publishedAt <= :now')
            ->orderBy('p.publishedAt', 'DESC')
            ->setParameter('now', new \DateTime())
            ->getQuery()->setMaxResults($amount)->getResult();
    }

    /**
     * @param $userId
     * @param int $amount
     *
     * @return array
     */
    public function findLatestByUserId($userId, $amount = 1)
    {
        return $this->createQueryBuilder('p')
            ->join('p.user', 'u')
            ->where('p.publishedAt <= :now')
            ->andWhere('u.id = :user_id')
            ->setParameter('user_id', $userId)
            ->orderBy('p.publishedAt', 'DESC')
            ->setParameter('now', new \DateTime())
            ->getQuery()->setMaxResults($amount)->getResult();
    }

    /**
     * @param $query
     *
     * @return array
     */
    public function findBySearchQuery($query)
    {
        $searchTerms = $this->extractSearchTerms($query);

        if (0 === count($searchTerms)) {
            return null;
        }

        $qb = $this->createQueryBuilder('p');

        foreach ($searchTerms as $key => $term) {
            $qb
                ->orWhere('p.title LIKE :param_' . $key)
                ->setParameter('param_' . $key, '%' . $term . '%');
        }

        return $qb
            ->orderBy('p.publishedAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery()->getResult();
    }

    public function findByTag($tag)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.tags', 't')
            ->where('t.name = :tag')
            ->setParameter('tag', $tag)
            ->getQuery();
    }

    /**
     * Returns the amount of Posts present in the DB.
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @return mixed
     */
    public function count()
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->select('count(s.id)')
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getSingleScalarResult();
    }

    /**
     * Returns the amount of Posts present in the DB for a given user.
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @param int $userId
     *
     * @return mixed
     */
    public function countByUserId(int $userId)
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->innerJoin('p.user', 'u')
            ->where('u.id = :user_id')
            ->setParameter('user_id', $userId)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getSingleScalarResult();
    }


    /**
     * Returns the amount of Posts present in the DB for each month (over the last 12 months max).
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @return mixed
     */
    public function countPerMonth()
    {
        $qb = $this->createQueryBuilder('p');

        $results = $qb->select('MONTH(p.publishedAt) AS month')
            ->addSelect('COUNT(p.publishedAt) as n')
            ->groupBy('p.publishedAt')
            ->groupBy('month')
            ->where('p.publishedAt > :date')
            ->setParameter('date', date('Y-m-t', strtotime("-1 year")))
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getResult();

        return $this->addZeroValuedMonths($results);
    }

    /**
     * Returns the amount of Posts present in the DB for each month (over the last 12 months max).
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @param int $userId
     *
     * @return array
     */
    public function countPerMonthByUserId(int $userId)
    {
        $results = $this->createQueryBuilder('p')
            ->select('MONTH(p.publishedAt) AS month')
            ->addSelect('COUNT(p.publishedAt) as n')
            ->innerJoin('p.user', 'u')
            ->where('u.id = :user_id')
            ->groupBy('p.publishedAt')
            ->setParameter('user_id', $userId)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getResult();

        return $this->addZeroValuedMonths($results);
    }

    /**
     * Splits the search query into terms.
     *
     * @param string $searchQuery
     *
     * @return array
     */
    private function extractSearchTerms($searchQuery)
    {
        $terms = array_unique(explode(' ', mb_strtolower($searchQuery)));

        return array_filter($terms,
            function ($term) {
                return 2 <= mb_strlen($term);
            }
        );
    }

    /**
     * Will add zeroes for months (in the last 12 months) if not returned by database
     *
     * Will accept results in the following format:
     * [
     *     [
     *         "month" => “10"    # number of the month
     *         "n" => "1"         # the amount of records in that month
     *    ]
     *    [ ... ]
     * ]
     *
     * @param $results  # array of results (must have format as above)
     *
     * @return array
     */
    public function addZeroValuedMonths($results)
    {
        $no_results = (0 === count($results));

        $month = time();
        for ($i = 0; $i < 12; $i++) {
            $month = strtotime('next month', $month);
            $numberOfMonth = date('m', $month);

            if (!$no_results && (int)$results[$i]['month'] !== (int)$numberOfMonth) {
                $toInsert = [['month' => $this->monthNumberToName($numberOfMonth), 'n' => '0']];
                $results = array_merge(array_slice($results, 0, $i), $toInsert, array_slice($results, $i));
            } elseif (!$no_results) {
                $results[$i]['month'] = $this->monthNumberToName($results[$i]['month']);
            } else {
                $results[] = [
                    'month' => $this->monthNumberToName($numberOfMonth),
                    'n' => '0',
                ];
            }
        }

        return $results;
    }

    /**
     * Converts a number of the month (1-12) and converts it to the name of that given month
     *
     * @param int $number # number of the month
     *
     * @return string
     */
    public function monthNumberToName(int $number)
    {
        return \DateTime::createFromFormat('!m', $number)->format('M');
    }
}
