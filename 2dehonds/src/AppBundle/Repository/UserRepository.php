<?php

namespace AppBundle\Repository;

/**
 * UserRepository
 *
 * @author Matthias Seghers
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function countShelterById($userId)
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.shelter)')
            ->where('u.id = :user_id')
            ->setParameter(':user_id', $userId)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getSingleScalarResult();
    }
}
