<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * AnimalRepository
 *
 * @author Matthias Seghers
 */
class AnimalRepository extends EntityRepository
{
    public function findRandom($amount = 6)
    {
        //TODO: Get certain amount of random elements from database
    }

    /**
     * Fetches the latest animal added to the database.
     * Has an optional parameter to set amount of results to fetch.
     *
     * @param int $amount
     *
     * @return mixed
     */
    public function findLatest($amount = 1)
    {
        return $this->createQueryBuilder('a')
            ->where('a.publishedAt <= :now')
            ->orderBy('a.publishedAt', 'DESC')
            ->setParameter('now', new \DateTime())
            ->getQuery()->setMaxResults($amount)->getResult();
    }

    /**
     * Fetches the latest animal added to the database for a given shelter.
     * Has an optional parameter to set amount of results to fetch.
     *
     * @param int $shelterId
     * @param int $amount
     *
     * @return mixed
     */
    public function findLatestByShelterId(int $shelterId, $amount = 1)
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.shelter', 's')
            ->where('a.publishedAt <= :now')
            ->andWhere('s.id = :shelter_id')
            ->setParameter(':shelter_id', $shelterId)
            ->orderBy('a.publishedAt', 'DESC')
            ->setParameter('now', new \DateTime())
            ->getQuery()->setMaxResults($amount)->getResult();
    }

    /**
     * Fetches all animals in given shelter (by ID)
     */
    public function findByShelterId(int $shelterId)
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.shelter', 's')->addSelect('s')
            ->where('s.id = :id')->setParameter('id', $shelterId)
            ->getQuery()->getResult();
    }

    /**
     *
     * Fetch all animals from shelter of given user.
     *
     * @param User $user
     *
     * @return array
     */
    public function findByUser(User $user)
    {
        return $this->findByShelterId($user->getShelter()->getId());
    }

    public function findByFilter(array $queryString)
    {
        $parameters = [];

        $qb = $this->createQueryBuilder('a');

        if (array_key_exists('animalType', $queryString) && $queryString['animalType']) {
            $qb->innerJoin('a.animalType', 'at')->addSelect('at')
                ->where('at.id = :animalTypeId');
            $parameters['animalTypeId'] = $queryString['animalType'];
        }

        if (array_key_exists('youngChildren', $queryString) && $queryString['youngChildren']) {
            $qb->andWhere('a.youngChildren = :youngChildren');
            $parameters['youngChildren'] = $queryString['youngChildren'];
        }

        if (array_key_exists('children', $queryString) && $queryString['children']) {
            $qb->andWhere('a.children = :children');
            $parameters['children'] = $queryString['children'];
        }

        if (array_key_exists('dogs', $queryString) && $queryString['dogs']) {
            $qb->andWhere('a.dogs = :dogs');
            $parameters['dogs'] = $queryString['dogs'];
        }

        if (array_key_exists('cats', $queryString) && $queryString['cats']) {
            $qb->andWhere('a.cats = :cats');
            $parameters['cats'] = $queryString['cats'];
        }

        if (array_key_exists('chipped', $queryString) && $queryString['chipped']) {
            $qb->andWhere('a.chipped = :chipped');
            $parameters['chipped'] = $queryString['chipped'];
        }

        if (array_key_exists('vaccinated', $queryString) && $queryString['vaccinated']) {
            $qb->andWhere('a.vaccinated = :vaccinated');
            $parameters['vaccinated'] = $queryString['vaccinated'];
        }

        if (array_key_exists('sterilized', $queryString) && $queryString['sterilized']) {
            $qb->andWhere('a.sterilized = :sterilized');
            $parameters['sterilized'] = $queryString['sterilized'];
        }

        $qb->setParameters($parameters)
            ->getQuery()->getResult();

        return $qb;
    }

    /**
     * Returns the amount of Animals present in the DB.
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @return mixed
     */
    public function count()
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('count(a.id)')
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getSingleScalarResult();
    }

    /**
     *  Returns the amount of Animals present in the DB for a given shelter.
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @param int $shelterId # The id of the shelter to filter the animals by.
     *
     * @return array
     */
    public function countByShelterId(int $shelterId)
    {
        return $this->createQueryBuilder('a')
            ->select('COUNT(a.id)')
            ->innerJoin('a.shelter', 's')
            ->where('s.id = :shelter_id')
            ->setParameter('shelter_id', $shelterId)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getSingleScalarResult();
    }

    /**
     * Returns the amount of Animals present in the DB for each month (over the last 12 months max).
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @return mixed
     */
    public function countPerMonth()
    {
        $qb = $this->createQueryBuilder('a');

        $results = $qb
            ->select('MONTH(a.publishedAt) AS month')
            ->addSelect('COUNT(a.publishedAt) as n')
            ->groupBy('a.publishedAt')
            ->where('a.publishedAt > :date')
            ->setParameter('date', date('Y-m-t', strtotime("-1 year")))
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getResult();


        return $this->addZeroValuedMonths($results);
    }

    /**
     * Returns the amount of Animals present in the DB for each month (over the last 12 months max).
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @param int $shelterId # The id of the shelter to count the animals from
     *
     * @return mixed
     */
    public function countPerMonthByShelterId(int $shelterId)
    {
        $results = $this->createQueryBuilder('a')
            ->select('MONTH(a.publishedAt) AS month')
            ->addSelect('COUNT(a.publishedAt) as n')
            ->innerJoin('a.shelter', 's')
            ->where('s.id = :shelter_id')
            ->groupBy('month')
            ->setParameter('shelter_id', $shelterId)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getResult();

        return $this->addZeroValuedMonths($results);
    }

    /**
     * Will add zeroes for months (in the last 12 months) if not returned by database
     *
     * Will accept results in the following format:
     * [
     *     [
     *         "month" => “10"    # number of the month
     *         "n" => "1"         # the amount of records in that month
     *    ]
     *    [ ... ]
     * ]
     *
     * @param $results  # array of results (must have format as above)
     *
     * @return array
     */
    public function addZeroValuedMonths($results)
    {
        $no_results = (0 === count($results));

        $month = time();
        for ($i = 0; $i < 12; $i++) {
            $month = strtotime('next month', $month);
            $numberOfMonth = date('m', $month);

            if (!$no_results && (int)$results[$i]['month'] !== (int)$numberOfMonth) {
                $toInsert = [['month' => $this->monthNumberToName($numberOfMonth), 'n' => '0']];
                $results = array_merge(array_slice($results, 0, $i), $toInsert, array_slice($results, $i));
            } elseif (!$no_results) {
                $results[$i]['month'] = $this->monthNumberToName($results[$i]['month']);
            } else {
                $results[] = [
                    'month' => $this->monthNumberToName($numberOfMonth),
                    'n' => '0',
                ];
            }
        }

        return $results;
    }

    /**
     * Converts a number of the month (1-12) and converts it to the name of that given month
     *
     * @param int $number # number of the month
     *
     * @return string
     */
    public function monthNumberToName(int $number)
    {
        return \DateTime::createFromFormat('!m', $number)->format('M');
    }

}
