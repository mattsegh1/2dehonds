<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ShelterRepository
 *
 * @author Matthias Seghers
 */
class ShelterRepository extends EntityRepository
{
    /**
     * Fetches the latest animal added to the database.
     * Has an optional parameter to set amount of results to fetch.
     *
     * @param int $amount
     *
     * @return array
     */
    public function findLatest($amount = 1)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT s
                FROM AppBundle:Shelter s
                WHERE s.createdAt <= :now
                ORDER BY s.createdAt DESC
            '
            )
            ->setParameter('now', new \DateTime());

        return $query->setMaxResults($amount)->getResult();
    }

    public function findByDistance(float $lat, float $lon, float $distInKm)
    {
        return $this->createQueryBuilder('s')
            ->select('s')
            ->innerJoin('s.location', 'l')->addSelect('l')
            ->addSelect(
                '( 6371 * acos(cos(radians(' . $lat . '))' .
                '* cos( radians( l.lat ) )' .
                '* cos( radians( l.lon )' .
                '- radians(' . $lon . ') )' .
                '+ sin( radians(' . $lat . ') )' .
                '* sin( radians( l.lat ) ) ) ) AS HIDDEN distance'
            )
            ->having('distance <= :distance')
            ->setParameter('distance', $distInKm)
            ->orderBy('distance', 'ASC')
            ->getQuery()->getResult();
    }

    /**
     * Returns the amount of Shelters present in the DB.
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @return mixed
     */
    public function count()
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->select('count(s.id)')
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getSingleScalarResult();
    }
}
