<?php

namespace AppBundle\Repository;

/**
 * TagRepository
 *
 * @author Matthias Seghers
 */
class TagRepository extends \Doctrine\ORM\EntityRepository
{
    public function findPopular($maxItems = 7)
    {
        return $this->createQueryBuilder('t')
            ->select('COUNT(p) AS HIDDEN nPosts', 't')
            ->leftJoin('t.posts', 'p')
            ->orderBy('nPosts', 'DESC')
            ->groupBy('t')
            ->getQuery()
            ->setMaxResults($maxItems)
            ->getResult();
    }

    public function findTrending()
    {
        return $this->createQueryBuilder('t')
            ->select('COUNT(p) AS HIDDEN nPosts', 't')
            ->leftJoin('t.posts', 'p')
            ->orderBy('nPosts', 'DESC')
            ->groupBy('t')
            ->getQuery()
            ->getResult();
    }
}
