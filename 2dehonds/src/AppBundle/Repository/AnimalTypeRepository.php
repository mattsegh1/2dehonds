<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * AnimalTypeRepository
 *
 * @author Matthias Seghers
 */
class AnimalTypeRepository extends EntityRepository
{
    /**
     * Returns the amount of Shelters present in the DB.
     * This result gets cached for 3600ms (1 hour) to reduce server load.
     *
     * @return mixed
     */
    public function count()
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->select('count(s.id)')
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getSingleScalarResult();
    }

    public function countByShelterId(int $shelterId)
    {
        return $this->createQueryBuilder('at')
            ->select('COUNT(at.name)')
            ->innerJoin('at.animals', 'a')
            ->innerJoin('a.shelter', 's')
            ->where('s.id = :shelter_id')
            ->setParameter('shelter_id', $shelterId)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getSingleScalarResult();
    }
}
