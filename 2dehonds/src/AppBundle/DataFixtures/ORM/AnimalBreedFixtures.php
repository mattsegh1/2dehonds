<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\AnimalBreed;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * @author Matthias Seghers
 */
class AnimalBreedFixtures extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 5;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $animalBreed = new AnimalBreed();
        $animalBreed->setName('Labrador');
        $animalBreed->setDescription('De labrador retriever of kortweg labrador is een hondenras dat afstamt van de St. Johns-hond. Dat ras is vanaf midden 19e eeuw in Engeland gekruist met een aantal andere rassen, zoals de Gordon setter, de Spaniël, de Flatcoated retriever en de Chesapeake Bayretriever.'
        );
        $manager->persist($animalBreed);

        $this->addReference('TestAnimalBreed', $animalBreed); // Reference for the next Data Fixture(s).

        for ($i = 0; $i < 10; ++$i) {
            $animalBreed = new AnimalBreed();
            $manager->persist($animalBreed);
            $animalBreed->setName($faker->unique()->word);
            $animalBreed->setDescription($faker->words(3, true));
            $this->addReference("TestAnimalBreed-${i}", $animalBreed); // Reference for the next Data Fixture(s).
        }

        $manager->flush();
    }
}