<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\AnimalType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class AnimalTypeFixtures
 * @package AppBundle\DataFixtures\ORM
 *
 * @author Matthias Seghers
 */
class AnimalTypeFixtures extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 4;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $animalType = new AnimalType();
        $animalType->setName('Hond');
        $animalType->setDescription('De hond (Canis lupus familiaris) is een roofdier uit de familie van de hondachtigen (Canidae), en een gedomestificeerde ondersoort van de wolf. '
        );
        $manager->persist($animalType);

        $this->addReference('TestAnimalType', $animalType); // Reference for the next Data Fixture(s).

        for ($i = 0; $i < 10; ++$i) {
            $animalType = new AnimalType();
            $manager->persist($animalType);
            $animalType->setName($faker->unique()->word);
            $animalType->setDescription($faker->words(3, true));
            $this->addReference("TestAnimalType-${i}", $animalType); // Reference for the next Data Fixture(s).
        }

        $manager->flush();
    }
}