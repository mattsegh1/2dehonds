<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Constants\Constants;
use AppBundle\Entity\Animal;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Finder\Finder;

/**
 * @author Matthias Seghers
 */
class AnimalFixtures extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 6;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $finder = new Finder();
        $finder->files()->in($this->container->getParameter('image_directory'))->path('dummy/animals');
        //dump($finder);
        $imagePaths = [];
        foreach ($finder as $file) {
            $imagePaths[] = $file->getRelativePathname();
        }

        $animal = new Animal();
        $animal->setName('John');
        $animal->setSex('M');
        $animal->setBirthdate(DateTime::createFromFormat('d/m/Y', '23/05/2013'));
        $animal->setInfo('Een zeer lieve hond. Gaat graag wandelen en is zeer speels.');
        $animal->setYoungChildren(true);
        $animal->setChildren(true);
        $animal->setCats(true);
        $animal->setDogs(true);
        $animal->setChipped(true);
        $animal->setVaccinated(true);
        $animal->setSterilized(true);
        $animal->setWeight(3215);
        $animal->setSize(55);
        $animal->setImage($faker->randomElement($imagePaths));
        $animal->setAnimalBreed($this->getReference('TestAnimalBreed'));
        $animal->setAnimalType($this->getReference('TestAnimalType'));
        $animal->setShelter($this->getReference('TestShelter'));
        $manager->persist($animal);

        $this->addReference('TestAnimal', $animal); // Reference for the next Data Fixture(s).

        for ($i = 0; $i < 10; ++$i) {
            $animal = new Animal();
            $animal->setSex($faker->randomElement(array_flip(Constants::GENDER)));
            $animal->setName($faker->firstName($animal->getSex()));
            $animal->setBirthdate($faker->dateTimeThisDecade());
            $animal->setInfo($faker->text());
            $animal->setYoungChildren($faker->boolean());
            $animal->setChildren($faker->boolean());
            $animal->setCats($faker->boolean());
            $animal->setDogs($faker->boolean());
            $animal->setChipped($faker->boolean());
            $animal->setVaccinated($faker->boolean());
            $animal->setSterilized($faker->boolean());
            $animal->setWeight($faker->numberBetween('500', '4000'));
            $animal->setSize($faker->numberBetween('5', '70'));
            $animal->setImage($faker->randomElement($imagePaths));
            $animal->setAnimalBreed($this->getReference("TestAnimalBreed-${i}"));
            $animal->setAnimalType($this->getReference("TestAnimalType-${i}"));
            $animal->setShelter($this->getReference("TestShelter-${i}"));
            $manager->persist($animal);

            $this->addReference("TestAnimal-${i}", $animal); // Reference for the next Data Fixture(s).
        }


        $manager->flush(); // Persist all managed Entities.
    }
}