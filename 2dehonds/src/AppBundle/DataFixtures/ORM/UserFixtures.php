<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserFixtures
 * @package AppBundle\DataFixtures\ORM
 *
 * @author Matthias Seghers
 */
class UserFixtures extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $test_password = 'admin';
        $factory = $this->container->get('security.encoder_factory');

        $manager = $this->container->get('fos_user.user_manager');

        //$user = $manager->createUser();
        $user = new User();
        $user->setUsername('admin');
        $user->setPlainPassword($test_password);
        $user->setEmail('admin@example.com');
        $user->setFirstname('John');
        $user->setLastname('Doe');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEnabled(true);
        $user->setShelter($this->getReference('TestShelter'));
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $user->setPassword($password);
        $manager->updateUser($user);
        $this->addReference('user.admin', $user);

        $test_password = 'admin';
        $factory = $this->container->get('security.encoder_factory');

        $manager = $this->container->get('fos_user.user_manager');

        $user = new User();
        $user->setUsername('demo_user');
        $user->setPlainPassword('demo_password');
        $user->setEmail('demo@test.com');
        $user->setFirstname('Foo');
        $user->setLastname('Bar');
        $user->setRoles([]);
        $user->setEnabled(true);
        $user->setShelter($this->getReference('TestShelter-1'));
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $user->setPassword($password);
        $manager->updateUser($user);
        $this->addReference('user.demo', $user);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 3;
    }
}