<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Shelter;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class ShelterFixtures
 * @package AppBundle\DataFixtures\ORM
 *
 * @author Matthias Seghers
 */
class ShelterFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $shelter = new Shelter();
        $shelter->setName('Top Asiel');
        $shelter->setDescription('Dit is een top asiel');
        $shelter->setLocation($this->getReference('TestLocation'));

        $manager->persist($shelter);

        $this->addReference('TestShelter', $shelter); // Reference for the next Data Fixture(s).

        for ($i = 0; $i < 10; ++$i) {
            $shelter = new Shelter();
            $manager->persist($shelter);
            $shelter->setName($faker->unique()->company);
            $shelter->setDescription($faker->words(3, true));
            $shelter->setLocation($this->getReference("TestLocation-${i}"));
            $this->addReference("TestShelter-${i}", $shelter); // Reference for the next Data Fixture(s).
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 2;
    }
}