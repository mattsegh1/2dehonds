<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Location;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class LocationFixtures
 * @package AppBundle\DataFixtures\ORM
 *
 * @author Matthias Seghers
 */
class LocationFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $location = new Location();
        $location->setAddress('Industrieweg 232, 9030 Mariakerke');
        $location->setCountry('BE');
        $location->setPostalCode('9030');
        $location->setCity('Mariakerke');
        $location->setStreet('Industrieweg 232');
        $location->setLat('51.086784');
        $location->setLon('3.671661');
        $location->setProvince('East Falanders');
        $manager->persist($location);

        $this->addReference('TestLocation', $location); // Reference for the next Data Fixture(s).

        for ($i = 0; $i < 10; ++$i) {
            $location = new Location();
            $manager->persist($location);
            $location->setCountry('BE'); // Currently we tailor to users in Belgium.
            $location->setPostalCode($faker->postcode);
            $location->setCity($faker->city);
            $location->setStreet($faker->streetAddress);
            $location->setAddress(
                $location->getStreet() . ', ' . $location->getPostalCode() . ' ' . $location->getCity()
            );
            $location->setLat($faker->latitude);
            $location->setLon($faker->latitude);
            $location->setProvince($faker->word);
            $this->addReference("TestLocation-${i}", $location); // Reference for the next Data Fixture(s).
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 1;
    }
}