<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;
use Cocur\Slugify\Slugify;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class PostFixtures
 * @package AppBundle\DataFixtures\ORM
 *
 * @author Matthias Seghers
 */
class PostFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $slugify = new Slugify();


        $post = new Post();
        $post->setTitle('Eerste post');
        $post->setSlug($slugify->slugify($post->getTitle()));
        $post->setText('Dit is mijn eerste post. Hello World!');
        $post->setUser($this->getReference('user.admin'));

        $tag = new Tag();
        $tag->setName("test");
        $post->addTag($tag);


        $manager->persist($post);

        $this->addReference('TestPost', $post); // Reference for the next Data Fixture(s).

        for ($i = 0; $i < 10; ++$i) {
            $post = new Post();
            $post->setTitle($faker->sentence());
            $post->setSlug($slugify->slugify($post->getTitle()));
            $post->setText($faker->sentences($faker->numberBetween(5, 30), true));
            $post->setUser($this->getReference('user.admin'));

            $tag = new Tag();
            $tag->setName($faker->word);
            $post->addTag($tag);

            $manager->persist($post);

            $this->addReference("TestPost-${i}", $post); // Reference for the next Data Fixture(s).
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 7;
    }
}