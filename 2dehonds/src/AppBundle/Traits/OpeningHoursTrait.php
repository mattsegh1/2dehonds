<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Spatie\OpeningHours\OpeningHours;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait OpeningHoursTrait
 *
 * @author Matthias Seghers
 */
trait OpeningHoursTrait
{
    /**
     * @ORM\Column(name="opening_hours", type="json", nullable=true)
     * @Assert\NotBlank(groups={"Default"});
     * @var string
     */
    protected $openingHours;

    /**
     * @param array $openingHours
     */
    public function setOpeningHours($openingHours)
    {
        $this->openingHours = $openingHours;
    }

    /**
     * @return OpeningHours
     */
    public function getOpeningHours()
    {
        return $this->openingHours;
    }

    /**
     * @return array
     */
    public function getDefaultOpeningHours()
    {
        return [
            'monday' => [
                '09:00-12:00',
                '13:00-18:00',
            ],
            'tuesday' => [
                '09:00-12:00',
                '13:00-18:00',
            ],
            'wednesday' => [
                '09:00-12:00',
            ],
            'thursday' => [
                '09:00-12:00',
                '13:00-18:00',
            ],
            'friday' => [
                '09:00-12:00',
                '13:00-18:00',
            ],
            'saturday' => [],
            'sunday' => [],
            'exceptions' => [],
        ];
    }
}