<?php

namespace AppBundle\Constants;

final class Constants
{
    const GENDER = [
        'M' => 'label.male',
        'F' => 'label.female'
    ];
}