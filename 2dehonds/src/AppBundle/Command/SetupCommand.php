<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * A console command to set up the project.
 *
 * To use this command, use execute the following code in a terminal window:
 *
 *     $ php bin/console 2dehonds:setup
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console 2dehonds:setup -vv
 *
 * @author Matthias Seghers
 */
class SetupCommand extends ContainerAwareCommand
{
    const MAX_ATTEMPTS = 5;

    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('2dehonds:setup')
            ->setDescription('Set up the project')
            ->setHelp($this->getCommandHelp());
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$progressBar = new ProgressBar($output, 2);
        //$progressBar->setBarCharacter('#');
        //$progressBar->setProgressCharacter("#");

        $application = $this->getApplication();

        $commands = [
            '2dehonds:db:reset' => null,
            'doctrine:fixtures:load' => null,
        ];

        try {
            foreach ($commands as $commandName => $commandParameters) {
                $parameters = [
                    'command' => $commandName,
                ];
                if (is_array($commandParameters)) {
                    foreach ($commandParameters as $commandParameter => $value) {
                        $parameters[$commandParameter] = $value;
                        dump($value);
                    }
                }
                $commandInput = new ArrayInput($parameters);

                $application
                    ->find($commandName)
                    ->run($commandInput, $output);

                //$progressBar->advance();
            }

            $this->io->success('Project has been installed!');
        }
        catch (\Exception $e) {
            $this->io->error('Something went wrong during the setup. Command returned: ' . $e->getMessage());
        }
    }

    private function getCommandHelp()
    {
        return <<<'HELP'
The <info>%command.name%</info> sets up the project

The default command will only set up the schema of the database.

  <info>php %command.full_name%</info>

HELP;
    }
}