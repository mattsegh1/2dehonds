<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * A console command that initializes the database user.
 *
 * To use this command, use execute the following code in a terminal window:
 *
 *     $ php bin/console 2dehonds:db:user
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console 2dehonds:db:user -vv
 *
 * @author Matthias Seghers
 */
class DbUserCommand extends ContainerAwareCommand
{
    const MAX_ATTEMPTS = 5;

    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('2dehonds:db:user')
            ->setDescription('Creates a database user based on the configuration')
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        // Get variables from `app/config/parameters.yml`
        $dbName = $container->getParameter('database_name');
        $dbUsername = $container->getParameter('database_user');
        $dbPassword = $container->getParameter('database_password');
        $dbAdminUsername = $container->getParameter('database_admin_user');
        $dbAdminPassword = $container->getParameter('database_admin_password');

        // Add database user with all privileges on (nonexistent) database
        $sql = "GRANT ALL PRIVILEGES ON ${dbName}.* TO '${dbUsername}' IDENTIFIED BY '${dbPassword}'";
        $command = sprintf('MYSQL_PWD=%s mysql --user=%s --execute="%s"', $dbAdminPassword, $dbAdminUsername, $sql);
        exec($command);

        $this->io->success("Database user `${dbUsername}` created!");
    }

    private function getCommandHelp()
    {
        return <<<'HELP'
The <info>%command.name%</info> command initializes the dabase user:

  <info>php %command.full_name%</info>

HELP;
    }
}
