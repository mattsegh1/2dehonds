<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * A console command that drops the database.
 *
 * To use this command, use execute the following code in a terminal window:
 *
 *     $ php bin/console 2dehonds:db:drop
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console 2dehonds:db:drop -vv
 *
 *
 * @author Matthias Seghers
 */
class DbDropCommand extends ContainerAwareCommand
{
    const MAX_ATTEMPTS = 5;

    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('2dehonds:db:drop')
            ->setDescription('Drop the database');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $application = $this->getApplication();

        $commands = [
            'doctrine:database:drop' => ['--force' => true],
        ];

        foreach ($commands as $commandName => $commandParameters) {
            $parameters = [
                'command' => $commandName,
            ];
            if (is_array($commandParameters)) {
                foreach ($commandParameters as $commandParameter => $value) {
                    $parameters[$commandParameter] = $value;
                }
            }
            $commandInput = new ArrayInput($parameters);

            $application
                ->find($commandName)
                ->run($commandInput, $output);
        }

        $this->io->success(sprintf('Database "%s" was successfully dropped.',
                $container->getParameter('database_name')
            )
        );
    }
}
