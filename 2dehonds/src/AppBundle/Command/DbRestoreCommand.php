<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * A console command that restores the database with the latest backup created.
 *
 * To use this command, use execute the following code in a terminal window:
 *
 *     $ php bin/console 2dehonds:db:restore
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console 2dehonds:db:restore -vv
 *
 * @author Matthias Seghers
 */
class DbRestoreCommand extends ContainerAwareCommand
{
    const MAX_ATTEMPTS = 5;

    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('2dehonds:db:restore')
            ->setDescription('Restores the database from latest backup SQL dump')
            ->setHelp($this->getCommandHelp());
    }


    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        // Get variables from `app/config/parameters.yml`
        $dbName = $container->getParameter('database_name');
        $dbUsername = $container->getParameter('database_user');
        $dbPassword = $container->getParameter('database_password');
        $dbDumpPath = $container->getParameter('database_dump_path');

        $command = "MYSQL_PWD=${dbPassword} mysqldump --user=${dbUsername} ${dbName} < ${dbDumpPath}/latest.sql";
        exec($command);

        $this->io->success("Backup for database `${dbName}` restored!");
    }

    private function getCommandHelp()
    {
        return <<<'HELP'
The <info>%command.name%</info> command restores the database with the latest SQL dump backup:

  <info>php %command.full_name%</info>

HELP;
    }
}
