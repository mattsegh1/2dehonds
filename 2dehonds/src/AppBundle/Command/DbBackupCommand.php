<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * A console command that creates a backup of the database into an SQL file.
 *
 * To use this command, use execute the following code in a terminal window:
 *
 *     $ php bin/console 2dehonds:db:backup
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console 2dehonds:db:backup -vv
 *
 *
 * @author Matthias Seghers
 */
class DbBackupCommand extends ContainerAwareCommand
{
    const MAX_ATTEMPTS = 5;

    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('2dehonds:db:backup')
            ->setDescription('Backups the database by dumping SQL file')
            ->setHelp($this->getCommandHelp());
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        // Get variables from `app/config/parameters.yml`
        $dbName = $container->getParameter('database_name');
        $dbUsername = $container->getParameter('database_user');
        $dbPassword = $container->getParameter('database_password');
        $dbDumpPath = $container->getParameter('database_dump_path');

        // Create folder(s)
        $command = "mkdir -p ${dbDumpPath}";
        exec($command);

        // Create SQL database dump
        $command = "MYSQL_PWD=${dbPassword} mysqldump --user=${dbUsername} --databases ${dbName} > ${dbDumpPath}/latest.sql";
        exec($command);

        // Gzip and timestamp created SQL database dump
        $command = "gzip -cr ${dbDumpPath}/latest.sql > ${dbDumpPath}/\$(date +\"%Y-%m-%d_%H%M%S\").sql.gz";
        exec($command);

        $this->io->success(sprintf('Database "%s" backup was created.',
                $container->getParameter('database_name')
            )
        );
    }

    private function getCommandHelp()
    {
        return <<<'HELP'
The <info>%command.name%</info> command creates a backup SQL of the database:

  <info>php %command.full_name%</info>

HELP;
    }
}
