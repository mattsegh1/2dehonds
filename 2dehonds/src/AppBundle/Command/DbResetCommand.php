<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * A console command that drops the database and initializes it by creating the database and the database user.
 *
 * To use this command, use execute the following code in a terminal window:
 *
 *     $ php bin/console 2dehonds:db:backup
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console 2dehonds:db:backup -vv
 *
 *
 * @author Matthias Seghers
 */
class DbResetCommand extends ContainerAwareCommand
{
    const MAX_ATTEMPTS = 5;

    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('2dehonds:db:reset')
            ->setDescription('Drops database and runs project:db:init')
            ->addOption('migrate', null, InputOption::VALUE_NONE, 'Migrates Doctrine Migrations')
            ->addOption('seed', null, InputOption::VALUE_NONE, 'Loads Doctrine Fixtures')
            ->setHelp($this->getCommandHelp());
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        // Get variables from `app/config/parameters.yml`
        $dbName = $container->getParameter('database_name');

        $application = $this->getApplication();

        $commands = [
            '2dehonds:db:drop' => null,
            '2dehonds:db:init' => null,
        ];

        if ($input->getOption('migrate') || $input->getOption('seed')) {
            $options = [];

            if ($input->getOption('migrate')) {
                $options['--migrate'] = true;
            }

            if ($input->getOption('seed')) {
                $options['--seed'] = true;
            }

            $commands['2dehonds:db:init'] = $options;
        }

        foreach ($commands as $commandName => $commandParameters) {
            $parameters = [
                'command' => $commandName,
            ];
            if (is_array($commandParameters)) {
                foreach ($commandParameters as $commandParameter => $value) {
                    $parameters[$commandParameter] = $value;
                }
            }
            $commandInput = new ArrayInput($parameters);

            $application
                ->find($commandName)
                ->run($commandInput, $output);
        }

        $this->io->success("Database `${dbName}` reset!");
    }

    private function getCommandHelp()
    {
        return <<<'HELP'
The <info>%command.name%</info> command resets the database by dropping it and initializes the database by creating the database user and database:

The default command will only set up the schema of the database.

  <info>php %command.full_name%</info>

Using the <comment>--migrate</comment> option will also execute a migration to a specified version or the latest available version..

  <info>php %command.full_name%</info> <comment>--migrate</comment>
  
Using the <comment>--seed</comment> option will also seed the database with dummy data.

  <info>php %command.full_name%</info> <comment>--seed</comment>

HELP;
    }
}
