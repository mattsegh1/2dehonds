<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * A console command that initializes the database by creating the database and the database user.
 *
 * To use this command, use execute the following code in a terminal window:
 *
 *     $ php bin/console 2dehonds:db:init
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console 2dehonds:db:init -vv
 *
 * @author Matthias Seghers
 */
class DbInitCommand extends ContainerAwareCommand
{
    const MAX_ATTEMPTS = 5;

    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('2dehonds:db:init')
            ->setDescription('Initializes the database by creating database user and database')
            ->addOption('migrate', null, InputOption::VALUE_NONE, 'Migrates Doctrine Migrations')
            ->addOption('seed', null, InputOption::VALUE_NONE, 'Loads Doctrine Fixtures')
            ->setHelp($this->getCommandHelp());
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        // Get variables from `app/config/parameters.yml`
        $dbName = $container->getParameter('database_name');

        $application = $this->getApplication();

        $commands = [
            '2dehonds:db:user',
            'doctrine:database:create',
            'doctrine:schema:create',
        ];

        if ($input->getOption('migrate')) {
            $commands[] = 'doctrine:migrations:migrate';
        }

        if ($input->getOption('seed')) {
            $commands[] = 'doctrine:fixtures:load';
        }

        foreach ($commands as $commandName) {
            $parameters = [
                'command' => $commandName,
            ];
            $commandInput = new ArrayInput($parameters);

            if (in_array($commandName, ['doctrine:fixtures:load', 'doctrine:migrations:migrate'])) {
                $commandInput->setInteractive(false);
            }

            $application
                ->find($commandName)
                ->run($commandInput, $output);
        }

        $this->io->success("Database `${dbName}` initialized!");
    }

    private function getCommandHelp()
    {
        return <<<'HELP'
The <info>%command.name%</info> command initializes the database by creating the database user and database:

The default command will only set up the schema of the database.

  <info>php %command.full_name%</info>

Using the <comment>--migrate</comment> option will also execute a migration to a specified version or the latest available version..

  <info>php %command.full_name%</info> <comment>--migrate</comment>
  
Using the <comment>--seed</comment> option will also seed the database with dummy data.

  <info>php %command.full_name%</info> <comment>--seed</comment>

HELP;
    }
}
