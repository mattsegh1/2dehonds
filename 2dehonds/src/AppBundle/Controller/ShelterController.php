<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Shelter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller used to manage shelter content in the public site.
 *
 * @package AppBundle\Controller
 *
 * @Route("/shelter")
 *
 * @author Matthias Seghers
 */
class ShelterController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     * @Cache(smaxage="10")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->query->get('lat', 0) && $request->query->get('lon', 0)) {
            $lat = $request->query->get('lat', 0);
            $lon = $request->query->get('lon', 0);
            $shelters = $em->getRepository(Shelter::class)->findByDistance($lat, $lon, 20);
        } else {
            $shelters = $em->getRepository(Shelter::class)->findAll();
        }

        $paginator = $this->get('knp_paginator');

        $result = $paginator->paginate(
            $shelters,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('shelter/index.html.twig',
            [
                'shelters' => $result,
            ]
        );
    }

    /**
     * @Route("/{id}", name="app_shelter_show")
     * @Method("GET")
     */
    public function showAction(Shelter $shelter)
    {
        // Fixing order by day of week for the view.
        $shelter->setOpeningHours(
            array_replace(array_flip(['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']),
                $shelter->getOpeningHours()
            )
        );

        return $this->render('shelter/show.html.twig',
            [
                'shelter' => $shelter,
            ]
        );
    }

}
