<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller used to manage post content on the public site.
 *
 * @Route("/post")
 *
 * @author Matthias Seghers
 */
class PostController extends Controller
{
    /**
     * @Route("/", name="app_post_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $queryString = $request->get('q', '');
        if ('' != $queryString) {
            $posts = $em->getRepository(Post::class)->findBySearchQuery($queryString);
        } else {
            $posts = $em->getRepository(Post::class)->findAll();
        }

        $paginator = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $posts,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('post/index.html.twig',
            [
                'posts' => $posts,
            ]
        );
    }

    /**
     * @Route("/tagged/{tag}", name="app_post_tagged")
     * @Method("GET")
     */
    public function taggedIndexAction(Request $request, $tag)
    {
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository(Post::class)->findByTag($tag);

        $paginator = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $posts,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('post/index.html.twig',
            [
                'posts' => $posts,
            ]
        );
    }

    /**
     * @Route("/autocomplete", name="app_post_autocomplete")
     * @Method("GET")
     *
     * @return Response|JsonResponse
     */
    public function autocompleteAction(Request $request)
    {
        $query = $request->query->get('q', '');
        $posts = $this->getDoctrine()->getRepository(Post::class)->findBySearchQuery($query);

        $results = [];
        foreach ($posts as $post) {
            $results[] = [
                'title' => htmlspecialchars($post->getTitle()),
            ];
        }

        return $this->json($results);
    }

    /**
     * @Route("/{slug}", name="app_post_show")
     * @Method("GET")
     */
    public function showAction(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $tags = $em->getRepository(Tag::class)->findPopular();

        return $this->render('post/show.html.twig',
            [
                'post' => $post,
                'tags' => $tags,
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function popularTagsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tags = $em->getRepository(Tag::class)->findPopular();

        return $this->render('post/popular_tags.html.twig', ['tags' => $tags]);

    }

}
