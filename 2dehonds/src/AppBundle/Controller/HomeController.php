<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Animal;
use AppBundle\Entity\Shelter;
use AppBundle\Entity\AnimalType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Controller used to manage homepage content in the public site.
 *
 * @Route("/")
 *
 * @author Matthias Seghers
 */
class HomeController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     * @Cache(smaxage="10")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $randomAnimals = $em->getRepository(Animal::class)->findLatest(6);
        $nAnimals = $em->getRepository(Animal::class)->count();
        $randomShelters = $em->getRepository(Shelter::class)->findLatest(6);
        $animalTypes = $em->getRepository(AnimalType::class)->findAll();

        return $this->render('home/index.html.twig',
            [
                'nAnimals' => $nAnimals,
                'randomAnimals' => $randomAnimals,
                'randomShelters' => $randomShelters,
                'animalTypes' => $animalTypes,
            ]
        );
    }

}
