<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Animal;
use AppBundle\Form\SearchAnimalType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller that manages animal content on the public site.
 *
 * @Route("animal")
 *
 * @author Matthias Seghers
 */
class AnimalController extends Controller
{
    /**
     * @Route("/", name="app_animal_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        //Check if querystring for search is available. Fetch results accordingly.
        if ($request->query->get('search_animal')) {
            $animals = $em->getRepository(Animal::class)->findByFilter($request->query->get('search_animal'));
        } else {
            $animals = $em->getRepository(Animal::class)->findAll();
        }

        $paginator = $this->get('knp_paginator');

        $options = [
            'parameterBag' => $request->query->get('search_animal'),
        ];
        // Search form
        $searchForm = $this->createForm(SearchAnimalType::class, null, $options);

        $result = $paginator->paginate(
            $animals,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 9)
        );

        return $this->render('animal/index.html.twig',
            [
                'animals' => $result,
                'form' => $searchForm->createView(),
            ]
        );
    }

    /**
     * Finds and displays a animal entity.
     *
     * @Route("/{id}", name="app_animal_show")
     * @Method("GET")
     */
    public function showAction(Animal $animal)
    {
        //TODO: better way to sort this. (issue with openinghours storing in DB?)
        $animal->getShelter()->setOpeningHours(
            array_replace(array_flip(['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']),
                $animal->getShelter()->getOpeningHours()
            )
        );

        return $this->render('animal/show.html.twig',
            [
                'animal' => $animal,
            ]
        );
    }

}
