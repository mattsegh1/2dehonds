<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Animal;
use AppBundle\Form\AnimalType;
use AppBundle\Form\SearchFormType;
use AppBundle\Service\FileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller that manages animal content in the backend.
 *
 * @Route("admin/animal")
 * @Security("has_role('ROLE_USER')")
 *
 * @author Matthias Seghers
 */
class AnimalController extends Controller
{
    /**
     * Lists all animal entities.
     *
     * @Route("/", name="admin_animal_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em->getFilters()->disable('softdeleteable');
            $animals = $em->getRepository(Animal::class)->findAll();
        } else {
            $animals = $em->getRepository(Animal::class)->findByUser($this->getUser());
        }

        $searchForm = $this->createForm(SearchFormType::class, null);

        $paginator = $this->get('knp_paginator');

        $result = $paginator->paginate(
            $animals,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('admin/animal/index.html.twig',
            [
                'animals' => $result,
                'searchForm' => $searchForm->createView(),
            ]
        );
    }

    /**
     * Creates a new animal entity.
     *
     * @Route("/new", name="admin_animal_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $animal = new Animal();
        $form = $this->createForm(AnimalType::class, $animal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $animal->setShelter($this->getUser()->getShelter());
            $em->persist($animal);
            $em->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.saved',
                    [
                        '%name%' => $this->get('translator')->trans('label.animal', [], 'base'),
                    ]
                )
            );

            return $this->redirectToRoute('admin_animal_show', ['id' => $animal->getId()]);
        }

        return $this->render('admin/animal/new.html.twig',
            [
                'animal' => $animal,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a animal entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_animal_show")
     * @Method("GET")
     */
    public function showAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em->getFilters()->disable('softdeleteable');
        }

        $animal = $em->getRepository(Animal::class)->find($id);

        $this->denyAccessUnlessGranted('show', $animal);

        $restoreForm = $this->createRestoreForm($animal);
        $deleteForm = $this->createDeleteForm($animal);

        return $this->render('admin/animal/show.html.twig',
            [
                'animal' => $animal,
                'delete_form' => $deleteForm->createView(),
                'restore_form' => $restoreForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing animal entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_animal_edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('edit', animal)")
     */
    public function editAction(Request $request, Animal $animal)
    {
        $this->denyAccessUnlessGranted('edit', $animal);

        $animal->setImage(
            new File($this->getParameter('image_directory') . '/' . $animal->getImage())
        );

        $deleteForm = $this->createDeleteForm($animal);
        $editForm = $this->createForm(AnimalType::class, $animal);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            //$file = $animal->getImage();
            //$fileName = $fileUploader->upload($file);

            //$animal->setImage($fileName);

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.saved',
                    [
                        '%name%' => $this->get('translator')->trans('label.animal', [], 'base'),
                    ]
                )
            );

            return $this->redirectToRoute('admin_animal_edit', ['id' => $animal->getId()]);
        }

        return $this->render('admin/animal/edit.html.twig',
            [
                'animal' => $animal,
                'form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a animal entity.
     *
     * @Route("/{id}", name="admin_animal_delete")
     * @Method("DELETE")
     * @Security("is_granted('delete', animal)")
     */
    public function deleteAction(Request $request, Animal $animal)
    {
        $form = $this->createDeleteForm($animal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($animal);
            $em->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.deleted',
                    [
                        '%name%' => $this->get('translator')->trans('label.animal', [], 'base'),
                    ]
                )
            );
        }

        return $this->redirectToRoute('admin_animal_index');
    }

    /**
     * Restore a deleted animal entity.
     *
     * @Route("/{id}", name="admin_animal_restore")
     * @Method("POST")
     */
    public function restoreAction(Request $request, int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->disable('softdeleteable');
        $animal = $em->getRepository(Animal::class)->find($id);

        $form = $this->createRestoreForm($animal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $animal->setDeletedAt(null);
            $em->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.restored',
                    [
                        '%name%' => $this->get('translator')->trans('label.animal', [], 'base'),
                    ]
                )
            );
        }

        return $this->redirectToRoute('admin_animal_index');
    }

    /**
     * Creates a form to delete a animal entity.
     *
     * @param Animal $animal The animal entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Animal $animal)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_animal_delete', ['id' => $animal->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Creates a form to restore an animal entity.
     *
     * @param Animal $animal
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createRestoreForm(Animal $animal)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_animal_restore', ['id' => $animal->getId()]))
            ->setMethod('POST')
            ->getForm();
    }
}
