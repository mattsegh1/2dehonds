<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use AppBundle\Form\SearchFormType;
use Cocur\Slugify\Slugify;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller that manages post content in the backend.
 *
 * @Route("admin/post")
 * @Security("has_role('ROLE_USER')")
 *
 * @author Matthias Seghers
 */
class PostController extends Controller
{
    /**
     * Lists all post entities.
     *
     * @Route("/", name="admin_post_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $qb = $em->getRepository(Post::class)->createQueryBuilder('p');
            $em->getFilters()->disable('softdeleteable');
        } else {
            $userId = $this->getUser()->getId();
            //TODO: Move paginator and search to repository for cleaner controller code.
            //$posts = $em->getRepository('AppBundle:Post')->findAll();
            $qb = $em->getRepository(Post::class)->createQueryBuilder('p')
                ->innerJoin('p.user', 'u')->addSelect('u')
                ->where('u.id = :id')->setParameter('id', $userId);
        }

        if ($request->query->getAlnum('search')) {
            $qb->andWhere('p.title LIKE :name')
                ->setParameter('name', '%' . $request->query->getAlnum('search') . '%');
        }

        $query = $qb->getQuery();

        $searchForm = $this->createForm(SearchFormType::class, null);

        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('admin/post/index.html.twig',
            [
                'posts' => $result,
                'searchForm' => $searchForm->createView(),
            ]
        );
    }

    /**
     * Creates a new post entity.
     *
     * @Route("/new", name="admin_post_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $post->setUser($this->getUser());

        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $post->setSlug($slugify->slugify($post->getTitle()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.saved',
                    [
                        '%name%' => $this->get('translator')->trans('label.post', [], 'base'),
                    ]
                )
            );

            return $this->redirectToRoute('admin_post_show', ['slug' => $post->getSlug()]);
        }

        return $this->render('admin/post/new.html.twig',
            [
                'post' => $post,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a post entity.
     *
     * @Route("/{slug}", name="admin_post_show")
     * @Method("GET")
     */
    public function showAction(string $slug)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $em->getFilters()->disable('softdeleteable');
        }

        $post = $em->getRepository(Post::class)->findOneBy(['slug' => $slug]);

        $this->denyAccessUnlessGranted('show', $post);

        $restoreForm = $this->createRestoreForm($post);
        $deleteForm = $this->createDeleteForm($post);

        return $this->render('admin/post/show.html.twig',
            [
                'post' => $post,
                'delete_form' => $deleteForm->createView(),
                'restore_form' => $restoreForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing post entity.
     *
     * @Route("/{slug}/edit", name="admin_post_edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('edit', post)")
     */
    public function editAction(Request $request, Post $post)
    {
        $deleteForm = $this->createDeleteForm($post);
        $editForm = $this->createForm('AppBundle\Form\PostType', $post);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.saved',
                    [
                        '%name%' => $this->get('translator')->trans('label.post', [], 'base'),
                    ]
                )
            );

            return $this->redirectToRoute('admin_post_edit', ['slug' => $post->getSlug()]);
        }

        return $this->render('admin/post/edit.html.twig',
            [
                'post' => $post,
                'form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a post entity.
     *
     * @Route("/{id}", name="admin_post_delete")
     * @Method("DELETE")
     * @Security("is_granted('delete', post)")
     */
    public function deleteAction(Request $request, Post $post)
    {
        $form = $this->createDeleteForm($post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->getTags()->clear();

            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.deleted',
                    [
                        '%name%' => $this->get('translator')->trans('label.post', [], 'base'),
                    ]
                )
            );
        }

        return $this->redirectToRoute('admin_post_index');
    }

    /**
     * Restore a deleted post entity.
     *
     * @Route("/{id}", name="admin_post_restore")
     * @Method("POST")
     */
    public function restoreAction(Request $request, int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->disable('softdeleteable');
        $post = $em->getRepository(Post::class)->find($id);

        $form = $this->createRestoreForm($post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $post->setDeletedAt(null);
            $em->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.restored',
                    [
                        '%name%' => $this->get('translator')->trans('label.post', [], 'base'),
                    ]
                )
            );
        }

        return $this->redirectToRoute('admin_post_index');
    }

    /**
     * Creates a form to delete a post entity.
     *
     * @param Post $post The post entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Post $post)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_post_delete', ['id' => $post->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Creates a form to restore a post entity.
     *
     */
    private function createRestoreForm(Post $post)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_post_restore', ['id' => $post->getId()]))
            ->setMethod('POST')
            ->getForm();
    }
}
