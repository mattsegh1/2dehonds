<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Shelter;
use AppBundle\Form\SearchFormType;
use AppBundle\Form\ShelterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller that manages post content in the backend.
 *
 * @Route("admin/shelter")
 * @Security("has_role('ROLE_USER')")
 *
 * @author Matthias Seghers
 */
class ShelterController extends Controller
{
    /**
     * Lists all shelter entities.
     *
     * @Route("/", name="admin_shelter_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em->getFilters()->disable('softdeleteable');
            $qb = $em->getRepository(Shelter::class)->createQueryBuilder('s');

            if ($request->query->getAlnum('search')) {
                $qb->where('s.name LIKE :name')
                    ->setParameter('name', '%' . $request->query->getAlnum('search') . '%');
            }
        } else {
            $qb = $em->getRepository(Shelter::class)->createQueryBuilder('s')
                ->where('s.id = :shelterId')->setParameter('shelterId', $this->getUser()->getShelter());

            if ($request->query->getAlnum('search')) {
                $qb->where('s.name LIKE :name')
                    ->setParameter('name', '%' . $request->query->getAlnum('search') . '%');
            }
        };

        $query = $qb->getQuery();

        // Search form
        $searchForm = $this->createForm(SearchFormType::class, null);

        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        return $this->render('admin/shelter/index.html.twig',
            [
                'shelters' => $result,
                'searchForm' => $searchForm->createView(),
            ]
        );
    }

    /**
     * Creates a new shelter entity.
     *
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/new", name="admin_shelter_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $shelter = new Shelter();
        $form = $this->createForm(ShelterType::class, $shelter);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($shelter);
            $em->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.saved',
                    [
                        '%name%' => $this->get('translator')->trans('label.shelter', [], 'base'),
                    ]
                )
            );

            return $this->redirectToRoute('admin_shelter_show', ['id' => $shelter->getId()]);
        }

        return $this->render('admin/shelter/new.html.twig',
            [
                'shelter' => $shelter,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a shelter entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_shelter_show")
     * @Method("GET")
     */
    public function showAction(int $id)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $em->getFilters()->disable('softdeleteable');
        }

        $shelter = $em->getRepository(Shelter::class)->find($id);

        //$shelter = $em->getRepository(Shelter::class)->find($id);
        $this->denyAccessUnlessGranted('show', $shelter);

        $restoreForm = $this->createRestoreForm($shelter);
        $deleteForm = $this->createDeleteForm($shelter);

        // TODO: Better way to fix this 'bug'
        $shelter->setOpeningHours(
            array_replace(array_flip(['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']),
                $shelter->getOpeningHours()
            )
        );

        return $this->render('admin/shelter/show.html.twig',
            [
                'shelter' => $shelter,
                'delete_form' => $deleteForm->createView(),
                'restore_form' => $restoreForm->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing shelter entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_shelter_edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('edit', shelter)")
     */
    public function editAction(Request $request, Shelter $shelter)
    {
        $deleteForm = $this->createDeleteForm($shelter);
        $editForm = $this->createForm('AppBundle\Form\ShelterType', $shelter);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.saved',
                    [
                        '%name%' => $this->get('translator')->trans('label.shelter', [], 'base'),
                    ]
                )
            );

            return $this->redirectToRoute('admin_shelter_edit', ['id' => $shelter->getId()]);
        }

        return $this->render('admin/shelter/edit.html.twig',
            [
                'shelter' => $shelter,
                'form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Deletes a shelter entity.
     *
     * @Route("/{id}", name="admin_shelter_delete")
     * @Method("DELETE")
     * @Security("is_granted('delete', shelter)")
     */
    public function deleteAction(Request $request, Shelter $shelter)
    {
        $form = $this->createDeleteForm($shelter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($shelter);
            $em->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.deleted',
                    [
                        '%name%' => $this->get('translator')->trans('label.shelter', [], 'base'),
                    ]
                )
            );
        }

        return $this->redirectToRoute('admin_shelter_index');
    }

    /**
     * Restore a deleted shelter entity.
     *
     * @Route("/{id}", name="admin_shelter_restore")
     * @Method("POST")
     */
    public function restoreAction(Request $request, int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->disable('softdeleteable');
        $shelter = $em->getRepository(Shelter::class)->find($id);

        $form = $this->createRestoreForm($shelter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $shelter->setDeletedAt(null);
            $em->flush();

            $this->addFlash(
                'notice',
                $this->get('translator')->trans('notice.restored',
                    [
                        '%name%' => $this->get('translator')->trans('label.shelter', [], 'base'),
                    ]
                )
            );
        }

        return $this->redirectToRoute('admin_shelter_index');
    }

    /**
     * Creates a form to delete a shelter entity.
     *
     * @param Shelter $shelter The shelter entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Shelter $shelter)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_shelter_delete', ['id' => $shelter->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Creates a form to restore a shelter entity.
     *
     * @param Shelter $shelter The shelter entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createRestoreForm(Shelter $shelter)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_shelter_restore', ['id' => $shelter->getId()]))
            ->setMethod('POST')
            ->getForm();
    }
}
