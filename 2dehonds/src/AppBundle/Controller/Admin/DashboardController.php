<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Animal;
use AppBundle\Entity\AnimalType;
use AppBundle\Entity\Post;
use AppBundle\Entity\Shelter;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Controller that manages animal content in the backend.
 *
 * @Route("admin")
 * @Security("has_role('ROLE_USER')")
 *
 * @author Matthias Seghers
 */
class DashboardController extends Controller
{
    /**
     * @Route("/", name="admin_homepage")
     * @Method("GET")
     */
    public function homeAction(AuthorizationCheckerInterface $authChecker)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->disable('softdeleteable');

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            $count['animal'] = $em->getRepository(Animal::class)->count();
            $count['animalType'] = $em->getRepository(AnimalType::class)->count();
            $count['shelter'] = $em->getRepository(Shelter::class)->count();
            $count['post'] = $em->getRepository(Post::class)->count();
            $animalHistory = $em->getRepository(Animal::class)->countPerMonth();
            $postHistory = $em->getRepository(Post::class)->countPerMonth();
            $latestAnimals = $em->getRepository(Animal::class)->findLatest(5);
            $latestPosts = $em->getRepository(Post::class)->findLatest(5);
        } else {
            $count['animal'] = $em->getRepository(Animal::class)->countByShelterId($this->getUser()->getShelter()->getId());
            $count['animalType'] = $em->getRepository(AnimalType::class)->countByShelterId($this->getUser()->getShelter()->getId());
            $count['shelter'] = $em->getRepository(User::class)->countShelterById($this->getUser()->getId());
            $count['post'] = $em->getRepository(Post::class)->countByUserId($this->getUser()->getId());
            $animalHistory = $em->getRepository(Animal::class)->countPerMonthByShelterId($this->getUser()->getShelter()->getId());
            $postHistory = $em->getRepository(Post::class)->countPerMonthByUserId($this->getUser()->getId());
            $latestAnimals = $em->getRepository(Animal::class)->findLatestByShelterId($this->getUser()->getId(), 5);
            $latestPosts = $em->getRepository(Post::class)->findLatestByUserId($this->getUser()->getId(), 5);
        }

        return $this->render(':admin/dashboard:index.html.twig',
            [
                'count' => $count,
                'animalHistory' => $animalHistory,
                'postHistory' => $postHistory,
                'latestAnimals' => $latestAnimals,
                'latestPosts' => $latestPosts,
            ]
        );
    }

}
