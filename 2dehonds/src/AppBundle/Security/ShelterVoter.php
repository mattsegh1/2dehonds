<?php

namespace AppBundle\Security;

use AppBundle\Entity\Shelter;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * It grants or denies permissions for actions related to shelters
 *
 * @author Matthias Seghers
 */
class ShelterVoter extends Voter
{
    const SHOW = 'show';
    const EDIT = 'edit';
    const DELETE = 'delete';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        return $subject instanceof Shelter && in_array($attribute, [self::SHOW, self::EDIT, self::DELETE], true);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // Admin has a lot of power!
        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        // the user must be logged in; if not, deny access
        if (!$user instanceof User) {
            return false;
        }

        $shelter = $subject;

        switch ($attribute) {
            case self::SHOW:
                return $this->canView($shelter, $user);
            case self::EDIT:
                return $this->canEdit($shelter, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Shelter $shelter, User $user)
    {
        // if they can edit, they can view
        return $this->canEdit($shelter, $user);
    }

    private function canEdit(Shelter $shelter, User $user)
    {
        return $user === $shelter->getUser();
    }

}