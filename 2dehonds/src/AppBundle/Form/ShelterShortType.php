<?php

namespace AppBundle\Form;

use AppBundle\Entity\Shelter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ShelterShortType
 * @package AppBundle\Form
 *
 * @author Matthias Seghers
 */
class ShelterShortType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['label' => 'label.name']);
        $builder->add('description', TextType::class, ['label' => 'label.description']);
        $builder->add('location', LocationType::class, ['label' => 'label.location']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => Shelter::class,
                'translation_domain' => 'shelter',
                'cascade_validation' => true,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_shelter';
    }


}
