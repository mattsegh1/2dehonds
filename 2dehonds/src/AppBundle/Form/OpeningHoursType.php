<?php

namespace AppBundle\Form;

use AppBundle\Form\DataTransformer\OpeningHoursTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OpeningHoursType
 * @package AppBundle\Form
 *
 * @author Matthias Seghers
 */
class OpeningHoursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $week = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday',];
        $hours = range(0, 23);

        $parameters = [
            'mapped' => true,
            //'html5' => false,
            //'widget' => 'single_text',
            // 'with_seconds' => false,
            // 'view_timezone' => 'Europe/Brussels',
            // 'model_timezone' => 'Europe/Brussels',
        ];

        foreach ($week as $day) {
            $builder
                ->add(sprintf('%sStartAm', $day), TimeType::class, $parameters)
                ->add(sprintf('%sEndAm', $day), TimeType::class, $parameters)
                ->add(sprintf('%sStartPm', $day), TimeType::class, $parameters)
                ->add(sprintf('%sEndPm', $day), TimeType::class, $parameters);
        }

        $builder->addModelTransformer(new OpeningHoursTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'compound' => true,
                'allow_extra' => true,
            ]
        );
    }
}