<?php

namespace AppBundle\Form;

use AppBundle\Entity\Animal;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;

/**
 * Class SearchAnimalType
 * @package AppBundle\Form
 *
 * @author Matthias Seghers
 */
class SearchAnimalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $querystring = $options['parameterBag'];

        $builder->setMethod('GET');
        $builder->add('animalType',
            EntityType::class,
            [
                'label' => 'label.animal_type',
                'placeholder' => 'label.choose_animal_type',
                'class' => 'AppBundle\Entity\AnimalType',
                'choice_label' => 'name',
            ]
        );
        $builder->add('youngChildren',
            CheckboxType::class,
            [
                'required' => false,
                'attr' =>
                    (isset($querystring['youngChildren']) && $querystring['youngChildren'] == 1 ? ['checked' => 'checked'] : []),
            ]
        );
        $builder->add('children',
            CheckboxType::class,
            [
                'required' => false,
                'attr' =>
                    (isset($querystring['children']) && $querystring['children'] == 1 ? ['checked' => 'checked'] : []),
            ]
        );
        $builder->add('dogs',
            CheckboxType::class,
            [
                'required' => false,
                'attr' =>
                    (isset($querystring['dogs']) && $querystring['dogs'] == 1 ? ['checked' => 'checked'] : []),
            ]
        );
        $builder->add('cats',
            CheckboxType::class,
            [
                'required' => false,
                'attr' =>
                    (isset($querystring['cats']) && $querystring['cats'] == 1 ? ['checked' => 'checked'] : []),
            ]
        );
        $builder->add('chipped',
            CheckboxType::class,
            [
                'required' => false,
                'attr' =>
                    (isset($querystring['chipped']) && $querystring['chipped'] == 1 ? ['checked' => 'checked'] : []),
            ]
        );
        $builder->add('vaccinated',
            CheckboxType::class,
            [
                'required' => false,
                'attr' =>
                    (isset($querystring['vaccinated']) && $querystring['vaccinated'] == 1 ? ['checked' => 'checked'] : []),
            ]
        );
        $builder->add('sterilized',
            CheckboxType::class,
            [
                'required' => false,
                'attr' =>
                    (isset($querystring['sterilized']) && $querystring['sterilized'] == 1 ? ['checked' => 'checked'] : []),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => Animal::class,
                'csrf_protection' => false,
                'parameterBag' => null,
                'translation_domain' => 'animal',
            ]
        );
    }
}