<?php

namespace AppBundle\Form;

use AppBundle\Entity\Shelter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ShelterType
 * @package AppBundle\Form
 *
 * @author Matthias Seghers
 */
class ShelterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['label' => 'label.name']);
        $builder->add('description', TextareaType::class, ['label' => 'label.about']);
        $builder->add('location', LocationType::class);
        $builder->add('openingHours', OpeningHoursType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => Shelter::class,
                'cascade_validation' => true,
                'translation_domain' => 'shelter'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_shelter';
    }


}
