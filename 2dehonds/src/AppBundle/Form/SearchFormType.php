<?php

namespace AppBundle\Form;

use AppBundle\Entity\Animal;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchFormType
 * @package AppBundle\Form
 *
 * @author Matthias Seghers
 */
class SearchFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //dump($data);
        $builder->setMethod('GET');
        $builder->add('search',
            SearchType::class,
            [
                'attr' => [
                    'placeholder' => 'label.search',
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'csrf_protection' => false,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return null;
    }


}