<?php
// src/AppBundle/Form/RegistrationType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProfileType
 * @package AppBundle\Form
 *
 * @author Matthias Seghers
 */
class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', TextType::class, ['attr' => ['class' => 'test'], 'label' => 'label.first_name']);
        $builder->add('lastName', TextType::class, ['label' => 'label.last_name']);
        $builder->add('email', TextType::class, ['label' => 'label.email']);
        //$builder->add('shelter', ShelterType::class);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'admin_user_profile';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'translation_domain' => 'base',
            ]
        );
    }
}