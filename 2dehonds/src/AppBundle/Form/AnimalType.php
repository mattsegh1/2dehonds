<?php

namespace AppBundle\Form;

use AppBundle\Constants\Constants;
use AppBundle\Entity\Animal;
use AppBundle\Entity\AnimalBreed;
use AppBundle\Entity\AnimalType as AnimalFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AnimalType
 * @package AppBundle\Form
 *
 * @author Matthias Seghers
 */
class AnimalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',
            TextType::class,
            [
                'label' => 'label.name',
            ]
        );
        $builder->add('sex',
            ChoiceType::class,
            [
                'choices' => array_flip(Constants::GENDER),
                'label' => 'label.sex'
            ]
        );
        $builder->add('birthdate',
            DateType::class,
            [
                'widget' => 'single_text',
                'format' => 'd/M/yyyy',
                'html5' => false,
                'label' => 'label.birthdate',
            ]
        );
        $builder->add('youngChildren');
        $builder->add('children');
        $builder->add('dogs');
        $builder->add('cats');
        $builder->add('info',
            TextType::class,
            [
                'label' => 'label.info',
            ]
        );
        $builder->add('chipped');
        $builder->add('vaccinated');
        $builder->add('sterilized');
        $builder->add('weight',
            IntegerType::class,
            [
                'label' => 'label.weight',
            ]
        );
        $builder->add('size',
            IntegerType::class,
            [
                'label' => 'label.size',
            ]
        );
        $builder->add('animalBreed',
            EntityType::class,
            [
                'class' => AnimalBreed::class,
                'label' => 'label.animal_breed',
            ]
        );
        $builder->add('animalType',
            EntityType::class,
            [
                'class' => AnimalFormType::class,
                'label' => 'label.animal_type',
            ]
        );
        $builder->add('image', FileType::class, ['label' => 'Foto']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => Animal::class,
                'translation_domain' => 'animal',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_animal';
    }


}
