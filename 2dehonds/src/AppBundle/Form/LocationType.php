<?php

namespace AppBundle\Form;

use AppBundle\Entity\Location;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LocationType
 * @package AppBundle\Form
 *
 * @author Matthias Seghers
 */
class LocationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('address', TextType::class, ['label' => 'label.address']);
        $builder->add('country', TextType::class, ['label' => 'label.country']);
        $builder->add('postalCode', TextType::class, ['label' => 'label.postal']);
        $builder->add('city', TextType::class, ['label' => 'label.city']);
        $builder->add('street', TextType::class, ['label' => 'label.street']);
        $builder->add('lat', TextType::class, ['attr' => ['readonly' => true], 'label' => 'label.lat']);
        $builder->add('lon', TextType::class, ['attr' => ['readonly' => true], 'label' => 'label.lon']);
        $builder->add('province', TextType::class, ['label' => 'label.province']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => Location::class,
                'translation_domain' => 'base'
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_location';
    }


}
