<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PostType
 * @package AppBundle\Form
 *
 * @author Matthias Seghers
 */
class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',
            TextType::class,
            [
                'label' => 'label.title',
                'required' => true,
            ]
        );
        $builder->add('text',
            TextareaType::class,
            [
                'label' => 'label.text',
                'required' => true,
            ]
        );
        $builder->add('publishedAt',
            DateTimeType::class,
            [
                'label' => 'label.published_at',
                'required' => true,
            ]
        );
        $builder->add('tags',
            TagsType::class,
            [
                'label' => 'label.tags',
                'required' => true,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => 'AppBundle\Entity\Post',
                'translation_domain' => 'post',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_post';
    }


}
