<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class OpeningHoursTransformer implements DataTransformerInterface
{
    protected $week = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday',];

    public function transform($data)
    {
        $splitHours = [];
        $splitHoursDateTime = [];

        foreach ($this->week as $day) {
            $dayHours = $data[$day];
            foreach ($dayHours as $key => $value) {
                $split = explode('-', $value);
                $splitHours[sprintf('%sStart%s', $day, $key === 0 ? 'Am' : 'Pm')] = $split[0];
                $splitHours[sprintf('%sEnd%s', $day, $key === 0 ? 'Am' : 'Pm')] = $split[1];
            }
        }

        foreach ($splitHours as $key => $value) {
            $splitHoursDateTime[$key] = new \DateTime(sprintf('%s:00', $value));
        }

        return $splitHoursDateTime;
    }

    public function reverseTransform($data)
    {
        $result = [];
        foreach ($this->week as $day) {
            $startAm = $data[sprintf('%sStartAm', $day)]->format('H:i');
            $endAm = $data[sprintf('%sEndAm', $day)]->format('H:i');
            $startPm = $data[sprintf('%sStartPm', $day)]->format('H:i');
            $endPm = $data[sprintf('%sEndPm', $day)]->format('H:i');

            $result[$day] = [];

            if (
                $startAm == '00:00'
                && $startAm == '00:00'
                && $startAm == '00:00'
                && $startAm == '00:00'
            ) {
                continue;
            }

            $result[$day][] = [$startAm, $endAm];
            $result[$day][] = [$startPm, $endPm];

            foreach ($result[$day] as &$value) {
                $value = implode('-', $value);
            }
        }

        return $result;
    }
}