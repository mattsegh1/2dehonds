<?php

namespace AppBundle\Entity;

use AppBundle\Constants\Constants;
use AppBundle\Traits\TimestampTrait;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Animal
 *
 * @ORM\Table(name="animal")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnimalRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 *
 * @author Matthias Seghers
 */
class Animal
{
    use TimestampTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="name.blank")
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\Choice(callback="getGenders")
     * @Assert\NotBlank(message="animal.sex.blank")
     * @ORM\Column(name="sex", type="string", length=1)
     */
    private $sex;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(message="animal.birthdate.blank")
     * @Assert\Date(message="animal.birthdate.invalid")
     * @Assert\LessThanOrEqual(value="today", message="animal.birthdate.not_future")
     * @ORM\Column(name="birthdate", type="date")
     */
    private $birthdate;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="animal.info.blank")
     * @ORM\Column(name="info", type="string", length=255)
     */
    private $info;

    /**
     * @var bool
     *
     * @ORM\Column(name="young_children", type="boolean")
     */
    private $youngChildren;

    /**
     * @var bool
     *
     * @ORM\Column(name="children", type="boolean")
     */
    private $children;

    /**
     * @var bool
     *
     * @ORM\Column(name="dogs", type="boolean")
     */
    private $dogs;

    /**
     * @var bool
     *
     * @ORM\Column(name="cats", type="boolean")
     */
    private $cats;

    /**
     * @var bool
     *
     * @ORM\Column(name="chipped", type="boolean")
     */
    private $chipped;

    /**
     * @var bool
     *
     * @ORM\Column(name="vaccinated", type="boolean")
     */
    private $vaccinated;

    /**
     * @var bool
     *
     * @ORM\Column(name="sterilized", type="boolean")
     */
    private $sterilized;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="animal.weight.blank")
     * @Assert\GreaterThan(0)
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="animal.size.blank")
     * @Assert\GreaterThan(0)
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\File(mimeTypes={ "image/jpeg", "image/bmp", "image/png", "image/gif" })
     */
    private $image;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishedAt;

    // Members for relationships

    /**
     * Many Animals have One Breed.
     * @ORM\ManyToOne(targetEntity="AnimalBreed", inversedBy="animals")
     * @ORM\JoinColumn(name="animal_breed_id", referencedColumnName="id", nullable=false)
     */
    private $animalBreed;

    /**
     * Many Animals are of one Type.
     * @ORM\ManyToOne(targetEntity="AnimalType", inversedBy="animals")
     * @ORM\JoinColumn(name="animal_type_id", referencedColumnName="id", nullable=false)
     */
    private $animalType;

    /**
     * Many Animals have One Shelter.
     * @ORM\ManyToOne(targetEntity="Shelter", inversedBy="animals")
     * @ORM\JoinColumn(name="shelter_id", referencedColumnName="id", nullable=false)
     */
    private $shelter;

    /**
     * Animal constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());
        $this->setPublishedAt(new DateTime());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Animal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sex
     *
     * @param string $sex
     *
     * @return Animal
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return Animal
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return Animal
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set youngChildren
     *
     * @param boolean $youngChildren
     *
     * @return Animal
     */
    public function setYoungChildren($youngChildren)
    {
        $this->youngChildren = $youngChildren;

        return $this;
    }

    /**
     * Get youngChildren
     *
     * @return bool
     */
    public function getYoungChildren()
    {
        return $this->youngChildren;
    }

    /**
     * Set children
     *
     * @param boolean $children
     *
     * @return Animal
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return bool
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set dogs
     *
     * @param boolean $dogs
     *
     * @return Animal
     */
    public function setDogs($dogs)
    {
        $this->dogs = $dogs;

        return $this;
    }

    /**
     * Get dogs
     *
     * @return bool
     */
    public function getDogs()
    {
        return $this->dogs;
    }

    /**
     * Set cats
     *
     * @param boolean $cats
     *
     * @return Animal
     */
    public function setCats($cats)
    {
        $this->cats = $cats;

        return $this;
    }

    /**
     * Get cats
     *
     * @return bool
     */
    public function getCats()
    {
        return $this->cats;
    }

    /**
     * Set chipped
     *
     * @param boolean $chipped
     *
     * @return Animal
     */
    public function setChipped($chipped)
    {
        $this->chipped = $chipped;

        return $this;
    }

    /**
     * Get chipped
     *
     * @return bool
     */
    public function getChipped()
    {
        return $this->chipped;
    }

    /**
     * Set vaccinated
     *
     * @param boolean $vaccinated
     *
     * @return Animal
     */
    public function setVaccinated($vaccinated)
    {
        $this->vaccinated = $vaccinated;

        return $this;
    }

    /**
     * Get vaccinated
     *
     * @return bool
     */
    public function getVaccinated()
    {
        return $this->vaccinated;
    }

    /**
     * Set sterilized
     *
     * @param boolean $sterilized
     *
     * @return Animal
     */
    public function setSterilized($sterilized)
    {
        $this->sterilized = $sterilized;

        return $this;
    }

    /**
     * Get sterilized
     *
     * @return bool
     */
    public function getSterilized()
    {
        return $this->sterilized;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Animal
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set size
     *
     * @param integer $size
     *
     * @return Animal
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @param mixed $publishedAt
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    // Methods for relationships

    /**
     * @return mixed
     */
    public function getAnimalBreed()
    {
        return $this->animalBreed;
    }

    /**
     * @param mixed $animalBreed
     */
    public function setAnimalBreed($animalBreed)
    {
        $this->animalBreed = $animalBreed;
    }

    /**
     * @return mixed
     */
    public function getAnimalType()
    {
        return $this->animalType;
    }

    /**
     * @param mixed $animalType
     */
    public function setAnimalType($animalType)
    {
        $this->animalType = $animalType;
    }

    /**
     * @return mixed
     */
    public function getShelter()
    {
        return $this->shelter;
    }

    /**
     * @param mixed $shelter
     */
    public function setShelter($shelter)
    {
        $this->shelter = $shelter;
    }
    
    // Callback functions for validation

    public static function getGenders()
    {
        return array_flip(Constants::GENDER);
    }

}

