<?php

namespace AppBundle\Entity;

use AppBundle\Traits\OpeningHoursTrait;
use AppBundle\Traits\TimestampTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Shelter
 *
 * @ORM\Table(name="shelter")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShelterRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 *
 * @author Matthias Seghers
 */
class Shelter
{
    use TimestampTrait;
    use OpeningHoursTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="shelter.name.blank")
     * @Assert\Length(
     *     min=3,
     *     minMessage="shelter.name.short",
     *     max=70,
     *     maxMessage="shelter.name.long"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank(message="shelter.description.blank")
     * @Assert\Length(
     *     min=255,
     *     max=2000,
     *     minMessage="shelter.description.short",
     *     maxMessage="shelter.description.long"
     * )
     */
    private $description;

    // Variables for relationships.

    /**
     * One Shelter has One Location. (for now)
     * @ORM\OneToOne(targetEntity="Location", inversedBy="shelter", cascade={"persist"})
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=false)
     */
    private $location;

    // Properties for relationships
    /**
     * One Shelter has One User. (for now)
     * @ORM\OneToOne(targetEntity="User", mappedBy="shelter")
     */
    private $user;

    /**
     * One Shelter has Many Users
     * @ORM\OneToMany(targetEntity="Animal", mappedBy="shelter")
     */
    private $animals;

    /**
     * Shelter constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());

        $this->setOpeningHours($this->getDefaultOpeningHours());

        $this->setAnimals(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Shelter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Shelter
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    // Methods for relationships.

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    // Methods for relationships

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getAnimals()
    {
        return $this->animals;
    }

    /**
     * @param mixed $animals
     */
    public function setAnimals($animals)
    {
        $this->animals = $animals;
    }

}

