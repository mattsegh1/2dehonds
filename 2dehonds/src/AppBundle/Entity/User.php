<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 *
 * @author Matthias Seghers
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="user.first_name.blank")
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *     min=4,
     *     minMessage="user.first_name.short",
     *     max=64,
     *     maxMessage="user.first_name.long"
     * )
     */
    protected $firstName;

    /**
     * @Assert\NotBlank(message="user.last_name.blank")
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *     min=3,
     *     minMessage="user.last_name.short",
     *     max=64,
     *     maxMessage="user.last_name.long"
     * )
     */
    protected $lastName;

    // Properties for relationships

    /**
     * One User has One Shelter. (for now)
     * @ORM\OneToOne(targetEntity="Shelter", inversedBy="user",  cascade={"persist"})
     * @ORM\JoinColumn(name="shelter_id", referencedColumnName="id", nullable=false)
     */
    private $shelter;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user")
     */
    private $posts;

    public function __construct()
    {
        parent::__construct();
        $this->setPosts(new ArrayCollection());
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    // Methods for relationships

    /**
     * @return mixed
     */
    public function getShelter()
    {
        return $this->shelter;
    }

    /**
     * @param mixed $shelter
     */
    public function setShelter($shelter)
    {
        $this->shelter = $shelter;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }
}