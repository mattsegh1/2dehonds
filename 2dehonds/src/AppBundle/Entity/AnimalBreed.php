<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AnimalBreed
 *
 * @ORM\Table(name="animal_breed")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnimalBreedRepository")
 *
 * @author Matthias Seghers
 */
class AnimalBreed
{
    use TimestampTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="name.blank")
     * @Assert\Length(
     *     min=4,
     *     minMessage="name.too_short",
     *     max=70,
     *     maxMessage="name.too_long"
     * )
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     * @Assert\Length(
     *     min=4,
     *     minMessage="description.too_short",
     *     max=255,
     *     maxMessage="description.too_long"
     * )
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    // Members for Relationships

    /**
     * One Breed has Many Animals.
     * @ORM\OneToMany(targetEntity="Animal", mappedBy="animalBreed")
     */
    private $animals;

    /**
     * AnimalBreed constructor.
     */
    public function __construct()
    {
        $this->setAnimals(new ArrayCollection());

        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AnimalBreed
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AnimalBreed
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    // Methods for relationships

    /**
     * @return mixed
     */
    public function getAnimals()
    {
        return $this->animals;
    }

    /**
     * @param mixed $animals
     */
    public function setAnimals($animals)
    {
        $this->animals = $animals;
    }

    public function __toString()
    {
        return $this->name;
    }


}

