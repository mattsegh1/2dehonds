<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AnimalType
 *
 * @ORM\Table(name="animal_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnimalTypeRepository")
 *
 * @author Matthias Seghers
 */
class AnimalType
{
    use TimestampTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="animal_type.name.blank")
     * @Assert\Length(
     *     min=4,
     *     minMessage="animal.name.short",
     *     max=180,
     *     maxMessage="animal.name.long"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     * @Assert\NotBlank(message="animal_type.description.blank")
     */
    private $description;

    // Members for Relationships

    /**
     * One Breed has Many Animals.
     * @ORM\OneToMany(targetEntity="Animal", mappedBy="animalType")
     */
    private $animals;

    /**
     * AnimalType constructor.
     */
    public function __construct()
    {
        $this->setAnimals(new ArrayCollection());

        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AnimalType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AnimalType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    // Methods for relationships

    /**
     * @return mixed
     */
    public function getAnimals()
    {
        return $this->animals;
    }

    /**
     * @param mixed $animals
     */
    public function setAnimals($animals)
    {
        $this->animals = $animals;
    }

    public function __toString()
    {
        return $this->name;
    }


}

