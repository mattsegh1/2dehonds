(function () {
    /**
     * Initialize sidenav
     * @type {Element}
     */
    var elem = document.querySelector('.sidenav');
    if (elem) {
        var instance = M.Sidenav.init(elem);
    }

    /**
     * Initialize modal for delete.
     * @type {Element}
     */
    var deleteModalElement = document.querySelector('#confirmationModal');
    if (deleteModalElement) {
        var options = [];
        var deleteModal = M.Modal.init(deleteModalElement, options);
    }

    /**
     * Initializes delete form
     * @type {Element}
     */
    var deleteForm = document.querySelector('#delete-form');
    if (deleteForm) {
        document.querySelector('#delete-form').addEventListener('submit', function (e) {
            deleteModal.open();
            e.preventDefault();
            document.querySelector('#confirmationModal #btnDelete').addEventListener('click', function () {
                document.querySelector('#delete-form').submit();
            });
        });
    }

    /**
     * Initialize selects
     * @type {NodeList}
     */
    var selects = document.querySelectorAll('select');
    if (selects.length) {
        selects.forEach(function (elem) {
            var instance = new M.Select(elem);
        });
    }
    /**
     * Initialize datepickers
     * @type {Element}
     */
    var dates = document.querySelector('.datepicker');
    if (dates) {
        var options = {'format': 'dd/mm/yyyy'};
        var instance = new M.Datepicker(dates, options);
    }

    /**
     * Initialize chart for posts.
     */
    var ctx = document.getElementById("postChart");
    if (ctx) {
        ctxPost = ctx.getContext('2d');
        var posts = new Chart(ctxPost, {
            type: 'line',
            data: {
                labels: postMonths,
                datasets: [{
                    label: postLabel,
                    data: postData,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1,
                    lineTension: 0
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function (value) {
                                if (value % 1 === 0) {
                                    return value;
                                }
                            }
                        }
                    }]
                }
            }
        });
    }

    /**
     * Initialize chart for animals
     */
    var ctx = document.getElementById("animalChart");
    if (ctx) {
        ctxAnimal = ctx.getContext('2d');
        var animals = new Chart(ctxAnimal, {
            type: 'line',
            data: {
                labels: animalMonths,
                datasets: [{
                    label: animalLabel,
                    data: animalData,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1,
                    lineTension: 0,
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function (value) {
                                if (value % 1 === 0) {
                                    return value;
                                }
                            }
                        }
                    }]
                }
            }
        });
    }
})();