(function () {
    /**
     * Initialize sidenav
     * @type {Element}
     */
    var elem = document.querySelector('.sidenav');
    if (elem) {
        var instance = M.Sidenav.init(elem);
    }

    // Initialize select
    var elem = document.querySelectorAll('select');
    if (elem.length !== 0) {
        var instance = M.Select.init(elem);
    }

    /**
     * Initializes autocomplete
     */
    var elem = document.querySelector('.autocomplete');
    if (elem) {
        var options = {
            data: {},
            minLength: 3,
            limit: 10,
            onAutocomplete: function () {
                document.querySelector('form#search').submit();
            },
        };
        var instance = M.Autocomplete.init(elem, options);

        document.querySelector('.autocomplete').addEventListener('keyup', function () {
            if (this.value.length >= 3) {
                fetchPosts(this.value);
            }
            ;
        });
    }

    /**
     * Fetch posts via AJAX
     */
    function fetchPosts(searchQuery) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                //document.getElementById("livesearch").innerHTML = this.responseText;
                var data, autoCompleteData = {};
                JSON.parse(this.responseText).forEach(function (element) {
                    autoCompleteData[element['title']] = null;
                });
                instance.updateData(autoCompleteData);
                elem.blur();
                elem.focus();

            }
        };
        xhttp.open("GET", autocompletePath + searchQuery, true);
        xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhttp.send();
    }
})();