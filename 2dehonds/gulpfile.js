'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minify = require('gulp-minify'),
    csslint = require('gulp-csslint'),
    jshint = require('gulp-jshint');

var sassPaths = [
    'node_modules/materialize-css/sass'
];

gulp.task('default', ['watch']);

gulp.task('watch', function () {
    gulp.watch('./web/assets/js/*.js', ['minify']);
    gulp.watch('./web/assets/scss/*.scss', ['compile-css']);
});

gulp.task('check', function () {
    gulp.watch('./web/assets/js/*.js', ['jshint']);
})

gulp.task('compile-css', function () {
    return gulp.src('./web/assets/sass/main.scss')
        .pipe(sass({includePaths: sassPaths, outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('./web/css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }));
});

gulp.task('jshint', function () {
    return gulp.src('./web/assets/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('csslint', function () {
    gulp.src('./web/assets/sass/main.scss')
        .pipe(csslint())
        .pipe(csslint.formatter());
});

gulp.task('minify', function () {
    gulp.src('./web/assets/js/*.js')
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(gulp.dest('./web/js'))
});
